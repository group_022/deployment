from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, IntegerType, StringType
from pyspark.sql.functions import lit

spark = SparkSession.builder.appName("example1").getOrCreate()

data = [(1, 'Akshay', 23, 3000), (2, 'Arun', 25, 6000)]
schema = StructType([StructField('id',IntegerType()),\
                    StructField('Name',StringType()),\
                    StructField('Age',IntegerType()),\
                    StructField('Salary',IntegerType()),
                    ])


df = spark.createDataFrame(data, schema) 
df.show()
df.printSchema()

df.select(df.id, df.Name).show()
df.filter((df.Age) < 25 ).show()
df.sort(df.Salary.desc()).show()
df.withColumn("country", lit("india")).show()
df.withColumn("Total_pay", df.Salary * 2).show()
df.withColumn("datatype", df.Salary.cast('float')).show()
